.. title: OpenLP 3.0.2 "Smart Simeon" Released
.. slug: 2023/02/09/openlp-302-smart-simeon-released
.. date: 2023-02-09 18:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-302-smart-simeon-released.jpg

We're happy to announce another bug fix release in the 3.0.x series.

Known Issues
------------

These are the issues that we know about, and are busy working on.

.. warning:: **Ubuntu 22.04, Linux Mint**

   The main view in the web remote does not update automatically on Ubuntu 22.04 due to
   Ubuntu/Mint shipping an old version of python3-websockets. This can be fixed by
   downloading and installing an updated version of the package from `the official
   Ubuntu archive`_.

.. warning:: **Wayland on Linux**

   OpenLP at present does not behave well under Wayland so the recommendation is to run
   under X11. If you can't run in X11 (or prefer to run it in XWayland), you should
   start it with ``QT_QPA_PLATFORM`` environment variable set to ``xcb``, although the
   Main View from the Web Remote will not work.

.. warning:: **Video backgrounds**

   Video / streaming backgrounds and background audio in songs currently do not work
   together. You may experience either the video not working but the audio works, or
   the audio doesn't work while the video does.

.. warning:: **macOS**

   macOS reports OpenLP as damaged, due to the OpenLP app not being signed. If you encounter this
   problem, you can try the following solution:

   1. From Applications / Utilities run Terminal
   2. In Terminal, type in the following command::
           
       xattr -dr com.apple.quarantine /Applications/OpenLP.app
           
   3. In some cases, you may need to append ``sudo`` to the above command
   4. This should allow you to start OpenLP normally

.. warning:: **macOS on Apple Silicon Chips**

   The VLC integration does not work.

   Some people have reported success when downloading and installing the **Universal Binary** from
   the `VLC website`_, but we have not confirmed this.

   The OpenLP team does not have the funds to purchase an Apple Silicon Mac at the moment, but if
   you'd like to help out financially, you can do so on our `donate page`_.

.. warning:: **Issues with High DPI monitors on Windows and macOS**

   We are aware of issues that our Windows and macOS users are encountering with 4K monitors and
   other high resolutions when scaling is involved. We are currently investigating this.

   The OpenLP team does not have funds for purchasing things like 4K monitors, but if you'd like to
   help out finanically so that we can, you can do so on our `donate page`_.

Bug Fixes
---------

- Only show hash if song book number exists
- FIX: Missing looping for theme background videos
- Fix Songs' Topics media manager icon to be the same from the Song Maintenance dialog
- Add ability to return transposed item with service_item format to avoid duplicate calls on remote
- Fix OpenLyrics whitespaces being 'eaten' (again)
- Fixg service manager's list exception when pressing 'Left' keyboard key without any item selected
- Force the use of SqlAlchemy 1.4 for now
- Removing login requirement from transpose endpoint
- Handle verse ranges in BibleServer
- Fix up loading 2.9.x services
- Attempt to fix #1287 by checking for both str and bytes, and decoding bytes to unicode
- Add debugging for VLC and fix strange state.
- Display the closing progress dialog during plugin shutdown
- Fix an issue with the Worship Center Pro importer
- Fix white preview display when previewing presentations
- Fix an issue where the websockets server would try to shut down even when -w is supplied
- Use a simpler approach when creating a tmp file when saving service files

Download
--------

Head on over to the `downloads section of the website`_ to download version 3.0.2 now!

.. note:: **Linux users**

   Please note that distributions can take a few days to be updated. Please be patient
   while the packagers update the packages.

.. _the official Ubuntu archive: http://archive.ubuntu.com/ubuntu/pool/universe/p/python-websockets/python3-websockets_10.2-1_all.deb
.. _VLC website: https://www.videolan.org/vlc/download-macosx.html
.. _donate page: https://openlp.org/donate
.. _downloads section of the website: https://openlp.org/#downloads
