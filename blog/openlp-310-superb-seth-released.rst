.. title: OpenLP 3.1.0 "Superb Seth" Released
.. slug: 2024/02/29/openlp-310-superb-seth-released
.. date: 2024-02-29 12:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-310-superb-seth-released.jpg

Today we are proud to announce that OpenLP 3.1.0 "Superb Seth" has been released. On the surface, this might not
seem like such a big release, since there seem to be very few new features, but if you dig a little deeper, the
sheer number of bugs squashed tells another story. We have also made a number of changes under the hood to help
carry OpenLP into the future.

Since version 2.0, OpenLP has supported the concept of "plugins" or "addons", but due to some limitations, these
plugins have always had to be deeply integrated into OpenLP. With the release of 3.1, OpenLP now has support for
community plugins, so that users can write their own plugins without needing them to become part of OpenLP.

As more and more churches are using OpenLP in a distributed fashion, more and more people are running into issues
when using OpenLP from multiple locations at the same time. In order to ease their pain, we have implemented a
locking mechanism which prevents more than one user from using OpenLP at the exact same time. Hopefully this will
tide users over until OpenLP has a better mechanism for sharing data.


   It is because of Yahweh's loving kindnesses that we are not consumed, because his mercies don't fail. They are
   new every morning. Great is your faithfulness.

   "Yahweh is my portion," says my soul. "Therefore I will hope in him."
  
   Lamentations 3:22-24 WEB

Known Issues
------------

Please read through the following issues carefully, so as to reduce the burden on the OpenLP developers.

Windows 7 and 8 are no longer supported
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Microsoft does not support them anymore either.

In order to make use of newer technologies, features and optimizations in the libraries that OpenLP uses, we
have had to drop support for Windows 7 and 8. Microsoft stopped supporting Windows 7 in January 2020 and
Windows 8 in July 2023, and Python stopped supporting Windows 7 with the release of Python 3.9 in October 2020.

This also makes life easier for our tiny development team.

macOS incorrectly reports OpenLP is damaged
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

macOS reports OpenLP as damaged, due to the OpenLP app not being signed. If you encounter this problem, you can
try the following solution:

1. From Applications / Utilities run Terminal
2. In Terminal, type in the following command::
       
      xattr -dr com.apple.quarantine /Applications/OpenLP.app
       
3. In some cases, you may need to prepend ``sudo`` to the above command
4. This should allow you to start OpenLP normally

"VLC not installed" error on macOS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

OpenLP uses VLC for video and media playback. At this stage VLC is not bundled with OpenLP, so you will need to
download and install it yourself.

You can download VLC from the `VLC website`_.

Main View does update in Ubuntu 22.04, Linux Mint
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The main view in the web remote does not update automatically on Ubuntu 22.04 due to Ubuntu/Mint shipping an old
version of python3-websockets. This can be fixed by downloading and installing an updated version of the package
from `the official Ubuntu archive`_.

Wayland on Linux
~~~~~~~~~~~~~~~~

OpenLP at present does not behave well under Wayland so the recommendation is to run under X11. If you can't run
in X11 (or prefer to run it in XWayland), you should start it with ``QT_QPA_PLATFORM`` environment variable set
to ``xcb``, although the Main View from the Web Remote will not work.

Video backgrounds
~~~~~~~~~~~~~~~~~

Video / streaming backgrounds and background audio in songs currently do not work together. You may experience
either the video not working but the audio works, or the audio doesn't work while the video does.

Download
--------

To download OpenLP 3.1.0, go to the `Downloads </#downloads>`_ section of the homepage.

.. admonition:: **Please Note**
   :class: note

   Linux releases of OpenLP typically trail a little behind the Windows and macOS releases, due to our team not
   building them. Please be patient, and reach out to the package maintainers if the package is still not up to
   date after 2 or 3 weeks.


New Features
------------

* Support for Community Plugins 
* Implement a filelock for shared data folder.
* Add Datasoul song importer
* Add Footer Content as Extra First Slide

Enhancements
------------

* Better support for High DPI displays with fractional scaling
* Allow Wayland to set the window icon
* Made the Wordproject import more robust 
* Better handling of missing VLC 
* Better handling of attempts to load invalid SWORD folder or zip-file 
* Don't build manual, use online manual instead 
* Update some system messaging 
* Move "Live" / "Preview" and current item on one line 
* Add authors to Powerpraise importer 
* Add the list of associated songs to the delete dialog in the song maintenance form 
* New theme adjustments: Adding letter spacing to theme main area; adding line and letter spacing to footer 
* Implementing new message websocket endpoint for faster remote operation
* Migrate from FontAwesome4 to Material Design Icons v5.9.55 
* Highlighted slidecontroller buttons 
* Correct About references and remove unused bits
* Improve PowerPoint detection by trying to start the application instead of looking it up in the registry. 
* List view grid tests + horizontal scroll handler on small height 
* List view with grid view mode + using this list view within thememanager
* Defining theme manager view mode default as 'List' 
* Allow the remote interface update notification to be turned off. 

Bug Fixes
---------

* Invalidate the service item cache when the theme changes 
* Fix "wrapped C/C++ object of type QTreeWidgetItem has been deleted"
* Fix irregular service theme saving 
* Fix AuthorType not getting translated 
* Fix a PermissionError that occurs on Windows 10/11 
* Fix First Time Wizard loop on Windows 
* Fix traceback on bible import when no bible available 
* Fix external DB settings 
* Fix alerts 
* Fix trimming leading whitespaces 
* Ensure a path set in PathEdit is a Path instance 
* Inject String.replaceAll javascript implementation if needed into webengine when browsing SongSelect. 
* Do not start the same presentation again when it's already live. 
* Prevent key error when unblank screen at start of presentation. 
* Fix the multiselect in the images plugin 
* Fix saving of songs 
* Fix spelling in songimport.py 
* Bypass image db updates if the db has already been upgraded 
* Fix a couple of macOS issues 
* Change SongSelect import procedure to import when clicking download on webpage 
* Don't crash when a permission error is raised 
* Fix presentations not being able to return from Display Screen 
* Fix the deadlock on macos 
* Fix issue #1618 by ignoring the messages if the event loop is not running 
* Fix issue #1382 by waiting for the service_manager to become available, or giving up after 2 minutes 
* Try to fix an issue with MediaInfo perhaps returning a str instead of an int 
* Fix issue #1582 by running the search in the original thread 
* Try to fix an issue that only seems to happen on macOS 
* Allow loading the same presentation file multiple times from 2.4.x service file. 
* Fix endless loop at the end of a PowerPoint presentation 
* Fix song search by author 
* Fix issue #1297 by reducing the number by 1024 times 
* Ignore the thumbnails if the path doesn't exist (fixes #914) 
* Fix an issue where an item's parent is None 
* Fix the 415 errors due to a change in Werkzeug 
* Fix translations loading on linux system-wide installation 
* Add detection for presentation files that were uploaded from the cloud. 
* Fix Datasoul translate strings 
* Skip missing thumbnails when loading a service 
* Minor fix for EasyWorship import 
* Fix stopping and looping of videos

Under the Hood
--------------

* Replace appdirs with platformdirs 
* Update SQLAlchemy usage to be 2.x compatible 
* Migrate Images plugin to use shared folder code 
* Migrate to using SQLALchemy declarative in Songs, Bibles, Custom, Images, Alerts, Song Usage
* Fix portable builds by re-arranging when the settings are created 
* Fix bug in _has_header of Bible CSV import
* Fix issues with upgrading 2.9.x databases 
* Fix OpenLP startup by reordering statements 
* Check before initialising a None Bible 
* Make PathEdit handle None values 
* Spoof the SongSelect webengine user agent 
* Update AppVeyor for Mac to install Pyro5 instead of Pyro4 
* Silence error when shutting down threads 
* Re introduce the selective turning off logging - correctly this time. 
* Fix some issues with building on macOS 
* Make some forward compatibility changes 
* Refactor last instances of TestCase-based tests 
* Add test coverage for __main__.py and remove some unused files 
* Update appveyor.yml to use Python 3.11. 
* Fix an issue with the arguments of with_only_columns 
* Remove dependency on PIL since the latest version does not support PyQt5 
* Fixing freezing screenshot test 
* Fallback code for display screenshot code (used on '/main'  Web Remote) 
* Update resource generation for ARM64 platforms (e.g. Apple M2) 
* Enumeration Conversion 
* Upgrade to Pyro5 
* Update CI to use the GitLab container registry 
* Display Custom Scheme 
* Fix bug in icon definition - Typo only 
* Take account of VLC on macOS being bundled with OpenLP 
* VLC - Cleanup - Stopping and looping correctly.
* Fixing Images not being able to be inserted on Service 
* Reusable Media Toolbar 
* Adding foundational support to Footer per slide 
* Merge CustomXMLBuilder and CustomXMLParser 
* Fix tests on Windows failing due to MagicMock in Path 
* Fix selected=True not being set at new Transpose API Endpoint 
* Rework the songs settings, so that they're not as squashed. 
* Remove WebOb -- we don't need it

.. _the official Ubuntu archive: http://archive.ubuntu.com/ubuntu/pool/universe/p/python-websockets/python3-websockets_10.2-1_all.deb
.. _VLC website: https://www.videolan.org/vlc/download-macosx.html
