.. title: OpenLP 3.1.0 Release Candidate 2 "Jubilant Josiah"
.. slug: 2023/09/17/openlp-310-release-candidate-2
.. date: 2023-09-17 12:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-310-release-candidate-2.jpg

The OpenLP developers are pleased to announce the second release candidate of OpenLP 3.1. This release candidate
contains a multitude of bug fixes, a number of enhancements, and even a new feature or two. There are also a number of
changes under the hood which puts OpenLP in a good position for the future.

**Wait, what happened to the first release candidate?!**

We had some issues with the packaging of RC1, and we caught a couple regressions (bugs that cause a feature not to work
like it previously did), and so we had to build a new release.

   Rejoice in the Lord always! Again I will say, "Rejoice!" Let your gentleness be known to all men. The Lord is at
   hand. In nothing be anxious, but in everything, by prayer and petition with thanksgiving, let your requests be
   made known to God. And the peace of God, which surpasses all understanding, will guard your hearts and your
   thoughts in Christ Jesus.

   Philippians 4:4-7

.. note:: **Windows 7 is no longer supported!** (Microsoft does not support it either.)

   In order to make use of newer technologies, features and optimizations in the libraries that OpenLP uses, we have
   had to drop support for Windows 7. Microsoft stopped supporting Windows 7 in January 2020, and Python stopped
   supporting Windows 7 with the release of Python 3.9 in October 2020.

.. warning:: **Download on macOS is damaged**

   macOS reports OpenLP as damaged, due to the OpenLP app not being signed. If you encounter this
   problem, you can try the following solution:

   1. From Applications / Utilities run Terminal
   2. In Terminal, type in the following command::
           
       xattr -dr com.apple.quarantine /Applications/OpenLP.app
           
   3. In some cases, you may need to append ``sudo`` to the above command
   4. This should allow you to start OpenLP normally

.. warning:: **Video/media playback on macOS**

   OpenLP uses VLC for video and media playback. At this stage VLC is not bundled with OpenLP, so you will need to
   download and install it yourself.

   You can download VLC from the `VLC website`_.

.. warning:: **Ubuntu 22.04, Linux Mint**

   The main view in the web remote does not update automatically on Ubuntu 22.04 due to
   Ubuntu/Mint shipping an old version of python3-websockets. This can be fixed by
   downloading and installing an updated version of the package from `the official
   Ubuntu archive`_.

.. warning:: **Wayland on Linux**

   OpenLP at present does not behave well under Wayland so the recommendation is to run
   under X11. If you can't run in X11 (or prefer to run it in XWayland), you should
   start it with ``QT_QPA_PLATFORM`` environment variable set to ``xcb``, although the
   Main View from the Web Remote will not work.

.. warning:: **Video backgrounds**

   Video / streaming backgrounds and background audio in songs currently do not work
   together. You may experience either the video not working but the audio works, or
   the audio doesn't work while the video does.


Download
--------

.. note:: **Debian/Ubuntu builds will be coming soon**

   We will update this blog post once the Debian/Ubuntu package has been built

.. raw:: html

   <div class="text-center" style="margin-bottom: 2em">
    <div class="btn-group">
      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-windows"></i>
        Windows
        &nbsp;
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li><a href="https://get.openlp.org/3.1.0rc2/OpenLP-3.1.0rc2-x64.msi">64-bit Installer</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc2/OpenLPPortable_3.1.0.3002-x64.paf.exe">64-bit Portable</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc2/OpenLP-3.1.0rc2.msi">32-bit Installer</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc2/OpenLPPortable_3.1.0.3002-x86.paf.exe">32-bit Portable</a></li>
      </ul>
    </div>
    <div class="btn-group">
      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-apple"></i>
        macOS
        &nbsp;
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li><a href="https://get.openlp.org/3.1.0rc2/OpenLP-3.1.0rc2-x86_64.dmg">For Macs with Intel Processors</a></li>
        <li><a href="https://get.openlp.org/3.1.0rc2/OpenLP-3.1.0rc2-arm64.dmg">For Macs with Apple Processors</a></li>
      </ul>
    </div>
    <!-- a class="btn btn-ubuntu" href="https://get.openlp.org/3.1.0rc2/openlp_3.1.0rc2-1_all.deb">
      <img class="icon notranslate" src="/images/ubuntu-logo.png"/> Ubuntu
    </a>
    <a class="btn btn-debian" href="https://get.openlp.org/3.1.0rc2/openlp_3.1.0rc2-1_all.deb">
      <img class="icon notranslate" src="/images/debian-logo.png"/> Debian
    </a -->
    <a class="btn btn-fedora" href="https://copr.fedorainfracloud.org/coprs/trb143/OpenLP/">
      <img class="icon notranslate" src="/images/fedora-logo.png"/> Fedora
    </a>
    <a class="btn btn-success" href="https://get.openlp.org/3.1.0rc2/OpenLP-3.1.0rc2.tar.gz">
      <i class="fa fa-file-archive-o"></i> Source
    </a>
   </div>

New Features
------------
* Implement a filelock for shared data folder. 
* Add Datasoul song importer 
* Add Footer Content as Extra First Slide 

Enhancements
------------
* Add authors in powerpraise importer 
* Add the list of associated songs to the delete dialog in the song maintenance form 
* Update some system messaging to clarify severity of messages
* Change SongSelect import procedure to import when clicking download on webpage 
* New theme adjustments: Add letter spacing to main area; add line and letter spacing to footer 
* Allow the remote interface update notification to be turned off. 
* UI: Move "Live" / "Preview" and current item onto one line 
* Migrate from FontAwesome4 to Material Design Icons v5.9.55 
* Highlighted slidecontroller buttons 
* Rework the songs settings, so that they're not as squashed. 
* Updated translation strings.

Bugs Fixed
----------
* Fix presentations not being able to return from Display Screen 
* Fix the deadlock on macOS 
* Fix issue #1618 by ignoring the messages if the event loop is not running 
* Fix issue #1382 by waiting for the service_manager to become available, or giving up after 2m 
* Try to fix an issue with MediaInfo perhaps returning a str instead of an int 
* Fix issue #1582 by running the search in the original thread 
* Try to fix an issue that only seems to happen on macOS 
* Fix bug # 1601 by allowing loading the same presentation file multiple times from 2.4.x service file 
* Don't crash when a permission error is raised during generation of the sha256 hash
* Fix endless loop at the end of a PowerPoint presentation 
* Fix song search by author 
* Fix issue #1297 by reducing the number by 1024 times 
* Add detection for presentation files that were uploaded from the cloud. 
* Ignore the thumbnails if the path doesn't exist (fixes #914) 
* Fix for #1495 task: wrapped C/C++ object of type QTreeWidgetItem has been deleted 
* Fixing Images not being able to be inserted on Service 
* Minor fix for EasyWorship import 
* Improve Powerpoint detection by trying to start the application instead of looking it up in the registry. 
* Skip missing thumbnails when loading a service 
* Upgrade internal VLC processing to support looping and stooping of media correctly.

Under the Hood
--------------
* Upgraded to Python 3.11, Windows 10+
* Upgrade images plugin to use the same shared code as media and presentations
* Display API abstraction 
* Fix various building issues on macOS
* Refactor database models to use declarative modeling
* Refactor database queries to support SQLAlchemy 2.0+
* Remove dependency on PIL/Pillow since the latest version does not support PyQt5 
* Make the media toolbar reusable
* Bypass image db updates if the db has already been upgraded 
* Refactor last instances of TestCase-based tests 
* Update resource generation for ARM64 platforms (e.g. Apple M2) 
* Upgrade to Pyro5 
* Fix an issue where an item's parent is None 
* Fix the 415 errors due to a change in Werkzeug
* Update CI to use the GitLab container registry 
* Display Custom Scheme
* Implement new message websocket endpoint 
* Adding foundational support to Footer per slide 


.. _the official Ubuntu archive: http://archive.ubuntu.com/ubuntu/pool/universe/p/python-websockets/python3-websockets_10.2-1_all.deb
.. _VLC website: https://www.videolan.org/vlc/download-macosx.html
