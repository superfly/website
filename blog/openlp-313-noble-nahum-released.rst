.. title: OpenLP 3.1.3 "Noble Nahum" Released
.. slug: 2024/08/24/openlp-313-noble-nahum-released
.. date: 2024-08-24 15:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. previewimage: /cover-images/openlp-313-noble-nahum-released.jpg

Another few months has passed and another OpenLP bugfix release. We expect this to be the last release in the 3.1.x
release series and the main development focus will now shift to the next major release.


Bug Fixes
---------
 * Correct media auto start behaviour.
 * Updated translations
 * Deal with PyMuPDF installed as fitz_old
 * Fix that our songbeamer chord parsing was partly based on a malformed file leading to weird imports
 * Web API method "get_language" returns the two-letter ISO 639 language code instead of "en" in case the language in
   OpenLP is auto detected.
 * Add setting that live preview shows blank screen
 * Add Web API endpoint to delete service items
 * Use correct transition delay when screen is hidden
 * Always show contents in live preview panel
 * Display presentation slides in live preview panel when main display is hidden.
 * Handle Python versions which include release levels by ignoring the release level, and restricting the check to
   only the major, minor, and micro components of the full version number.
 * Prevent crash by adding check for existence of linked audio file.
 * Do not show screen change message twice
 * Fixed Issue 1584 (switching Windows user account crashes an active OpenLP instance)
 
 
Known Issues
------------

See the `OpenLP 3.1 release blog post </blog/2024/02/29/openlp-310-superb-seth-released>`_ for the list of known issues.
