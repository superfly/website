.. title: New Release: OpenLP 3.0 RC2 (2.9.5) Helpful Hadassah
.. slug: 2022/08/14/new-release-openlp-30-rc2-295-helpful-hadassah
.. date: 2022-08-14 12:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description:
.. type: text
.. previewimage: /cover-images/new-release-openlp-30-rc2-295-helpful-hadassah.jpg

Today we are announcing the release of OpenLP 2.9.5. This is the second release candidate of OpenLP 3.0, and contains a plethora of bug fixes.

.. warning::

   Unfortunately there is a bug in this release which causes an error when you edit a song. A
   temporary workaround for this is to enable chords in Settings -> Songs -> Enable Chords. In the
   light of this, we are working on a new release.

.. caution::

   Installing this version of OpenLP will override any other installation of OpenLP you already have.
   Please make a backup of your data!

.. note::

   macOS may report OpenLP as damaged. If you encounter this problem, you can try the following solution. From
   Applications / Utilities run Terminal. In Terminal, type in the following command::

      xattr -dr com.apple.quarantine /Applications/OpenLP.app

   We are working on a remedy for this problem, but this command should tie you over till we get it sorted out.

.. note::

   For Linux users, due to the immaturity of Wayland, OpenLP does not behave well and it is recommended
   that X11 is used as the display manager.

You can download this release from the bottom of this blog post.

    We know that all things work together for good for those who love God, to those who are called according to his purpose.

    Romans 8:28 WEB

Bug fixes in this release
-------------------------

* Fix various issues in downloading and maintaining Web Remote
* Fix various issues in saving services
* Fix various bugs in handling files
* Fix various bugs around background audio in songs
* Fix some formatting in songs/slides/etc
* Fix Crosswalk Bible importer
* Various bug fixes in the remote projector controller
* Fix various issues around videos, background images and blanking
* Switch to using the old way of making text outlines
* Change the icons from groups to folders in the Images plugin
* Fix various issues in chord handling
* Confirm deleting item from service
* And much, much, much more...

Downloads
---------

.. raw:: html

   <div class="text-center" style="margin-bottom: 2em">
    <div class="btn-group">
      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-windows"></i>
        Windows
        &nbsp;
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li><a href="https://get.openlp.org/2.9.5/OpenLP-2.9.5-x64.msi">64-bit Installer</a></li>
        <li><a href="https://get.openlp.org/2.9.5/OpenLPPortable_2.9.5.0-x64.paf.exe">64-bit Portable</a></li>
        <li><a href="https://get.openlp.org/2.9.5/OpenLP-2.9.5.msi">32-bit Installer</a></li>
        <li><a href="https://get.openlp.org/2.9.5/OpenLPPortable_2.9.5.0-x86.paf.exe">32-bit Portable</a></li>
      </ul>
    </div>
    <a class="btn btn-default" href="https://get.openlp.org/2.9.5/OpenLP-2.9.5.dmg">
      <i class="fa fa-apple"></i> macOS 10.12+
    </a>
    <a class="btn btn-ubuntu" href="https://get.openlp.org/2.9.5/openlp_2.9.5-1_all.deb">
      <img class="icon notranslate" src="/images/ubuntu-logo.png"/> Ubuntu
    </a>
    <a class="btn btn-debian" href="https://get.openlp.org/2.9.5/openlp_2.9.5-1_all.deb">
      <img class="icon notranslate" src="/images/debian-logo.png"/> Debian
    </a>
    <a class="btn btn-fedora" href="https://copr.fedorainfracloud.org/coprs/trb143/OpenLP/">
      <img class="icon notranslate" src="/images/fedora-logo.png"/> Fedora
    </a>
    <a class="btn btn-success" href="https://get.openlp.org/2.9.5/OpenLP-2.9.5.tar.gz"><i class="fa fa-file-archive-o"></i> Source</a>
   </div>
